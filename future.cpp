
// <future> 头文件中包含了以下几个类和函数：
// Providers 类：std::promise, std::package_task
// Futures 类：std::future, shared_future.
// Providers 函数：std::async()
// 其他类型：std::future_error, std::future_errc, std::future_status, std::launch.

#include <iostream>
#include <chrono>
#include <future>

using namespace std::chrono;

std::string GetDataFromDB(std::string recvData) 
{
    std::cout << "async[" << std::this_thread::get_id() << "] thread start" << std::endl;
    std::this_thread::sleep_for(seconds(5));
    return recvData.append("_Database");
}

// 注意async第一个参数的含义
// 1. std::launch::async 立即执行
// 2. std::launch::deferred 不执行直到对执行结果future调用get()
// 3. std::launch::async | std::launch::deferred 由操作系统决定，如果系统资源紧张则同步否则异步。默认参数。

// 返回结果future注意点
// deffered：异步操作还没有开始
// ready：异步操作已经完成
// timeout：异步操作超时

void async_demo()
{
	std::cout << "main[" << std::this_thread::get_id() << "] thread start" << std::endl;
	system_clock::time_point start = system_clock::now();

    // 第一个参数设置为launch::async强制开一个线程运行这个任务
    std::future<std::string> resultFromDB = std::async(std::launch::async, GetDataFromDB, "hello");
    std::future_status status;
	std::string dbData;
	do
	{
		status = resultFromDB.wait_for(std::chrono::seconds(1));
		switch (status)
		{
		case std::future_status::ready:
			std::cout << "Ready..." << std::endl;
			dbData = resultFromDB.get();
			std::cout << dbData << std::endl;
			break;
		case std::future_status::timeout:
			std::cout << "timeout..." << std::endl;
			break;
		case std::future_status::deferred:
			std::cout << "deferred..." << std::endl;
			break;
		default:
			break;
		}
 
	} while (status != std::future_status::ready);
 
	
	//获取结束时间
	auto end = system_clock::now();
 
	auto diff = duration_cast<seconds>(end - start).count();
	std::cout << "Total Time taken= " << diff << " Seconds" << std::endl;
}

void promise_demo()
{}

int main() {
    

}