// c++11 时间库
#include <chrono>
#include <iostream>
#include <iomanip>
#include <ctime>
using namespace std::chrono;

/*  获取时间戳的两种方法，c反而更简洁  
 *  chrono::system_clock::time_point与std::time_t类型可相互函数
 *  chrono::system_clock::to_time_t()   
 *  chrono::system_clock::from_time_t()     
 */
void MethodsToGetUnixTimeDemo()
{
    // 1. c-style
    std::time_t oldtime = time(NULL);

    // 2. c++11 style
    // 把system_clock::time_point转换为std::time_t
    system_clock::time_point now = system_clock::now();
    std::time_t newtime = system_clock::to_time_t(now);

    // output
    std::cout << "new time = " << newtime << " old time = " << oldtime << std::endl;

    // 3. convert to string format(threadunsafe)
    // std:: cout << "new time(format1) = " << std::put_time(std::localtime(&newtime), "%Y-%m-%d %X"); 

    // 4. convert to string format(threadsafe)
    // struct tm cutTm = {0};
    // std:: cout << "new time(format1) = " << std::put_time(localtime_r(&newtime, &cutTm), "%Y-%m-%d %X");
}

std::string GetTimeTDayDiff(const std::time_t& oldtime, const std::time_t& newtime) 
{
    double diff = difftime(oldtime, newtime);
    
}

int main()
{
    MethodsToGetUnixTimeDemo();
    return 1;
}