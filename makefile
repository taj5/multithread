
BIN_DIR = bin/
CUR_DIR = $(shell pwd)

all:condition_variable threadsafe_queue future chrono

condition_variable:condition_variable.cpp  | $(BIN_DIR)
	g++ $^ -o $(BIN_DIR)$@ -lpthread -std=c++11

threadsafe_queue:threadsafe_queue.cpp  | $(BIN_DIR)
	g++ $^ -o $(BIN_DIR)$@ -lpthread -std=c++11

future:future.cpp  | $(BIN_DIR)
	g++ $^ -o $(BIN_DIR)$@ -lpthread -std=c++11

chrono:chrono.cpp  | $(BIN_DIR)
	g++ $^ -o $(BIN_DIR)$@ -lpthread -std=c++11

bind:bind.cpp  | $(BIN_DIR)
	g++ $^ -o $(BIN_DIR)$@ -lpthread -std=c++11

traits:traits.cpp  | $(BIN_DIR)
	g++ $^ -o $(BIN_DIR)$@ -lpthread -std=c++11

timer:timer.cpp  | $(BIN_DIR)
	g++ $^ -o $(BIN_DIR)$@ -lpthread -std=c++11

$(BIN_DIR):
	@ mkdir -p $@
