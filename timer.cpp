// 
// Created by leoxae on 19-9-2. 
// 
#ifndef KEEKOAIROBOT_TIMERTASKHELPER_H 
#define KEEKOAIROBOT_TIMERTASKHELPER_H 
#include <functional> 
#include <chrono>
#include <thread>
#include <atomic>
#include <memory>
#include <mutex>
#include <condition_variable>
#include <iostream>
#include <vector>

class Timer{ 
    public: Timer() :expired_(true), try_to_expire_(false){ } 
    Timer(const Timer& t)
    { 
        expired_ = t.expired_.load(); 
        try_to_expire_ = t.try_to_expire_.load(); 
    } 
    ~Timer()
    { 
        // Expire(); 
        // std::cout << "timer destructed!" << std::endl; 
    }

    void StartTimer(int interval, std::function<void()> task)
    { 
        if (expired_ == false)
        { 
            std::cout << "timer is currently running, please expire it first..." << std::endl; 
            return;
        }
        expired_ = false;
        std::thread([this, interval, task]() {
                while (!try_to_expire_) {
                    std::this_thread::sleep_for(std::chrono::milliseconds(interval)); 
                    task(); 
                } 
                std::cout << "stop task..." << std::endl; 
                { 
                    std::lock_guard<std::mutex> locker(mutex_);
                    expired_ = true; 
                    expired_cond_.notify_one(); 
                }
                std::cout << "task stopped" << std::endl; 
        }).detach();

    }

    // void StartTimer(int interval, std::vector<std::function<void()>>& tasks) {
    //     std::thread([this, interval, task]() {
    //             while (!try_to_expire_) {
    //                 std::this_thread::sleep_for(std::chrono::milliseconds(interval)); 
    //                 task(); 
    //             } 
    //             std::cout << "stop task..." << std::endl; 
    //             { 
    //                 std::lock_guard<std::mutex> locker(mutex_);
    //                 expired_ = true; 
    //                 expired_cond_.notify_one(); 
    //             }
    //             std::cout << "task stopped" << std::endl; 
    //     }).detach();
    // }

    void Expire() { 
        if (expired_) {
            std::cout << "timer is expired return" << std::endl; 
            return; 
        } 
        if (try_to_expire_) { 
            std::cout << "timer is trying to expire, please wait..." << std::endl; 
            return; 
        } 
        try_to_expire_ = true; 
        { 
            std::unique_lock<std::mutex> locker(mutex_);
            std::cout << "wait condition" << std::endl;
            expired_cond_.wait(locker, [this]{return expired_ == true; }); 
            std::cout << "wait done" << std::endl;
            if (expired_ == true) { 
                std::cout << "timer expired!" << std::endl; 
                try_to_expire_ = false; 
            } 
        } 
    }

    // 同步执行一次定时任务
    template<typename callable, class... arguments> 
    void SyncWait(int after, callable&& f, arguments&&... args) 
    { 
        std::function<typename std::result_of<callable(arguments...)>::type()> task(std::bind(std::forward<callable>(f), std::forward<arguments>(args)...)); 
        std::this_thread::sleep_for(std::chrono::milliseconds(after)); 
        task();
    }

    // 异步执行一次定时任务
    template<typename callable, class... arguments> 
    void AsyncWait(int after, callable&& f, arguments&&... args) 
    { 
        std::function<typename std::result_of<callable(arguments...)>::type()> task(std::bind(std::forward<callable>(f), std::forward<arguments>(args)...)); 
        std::thread([after, task](){ 
            std::this_thread::sleep_for(std::chrono::milliseconds(after)); 
            task(); 
        }).detach(); 
    }

private: 
    std::atomic<bool> expired_; 
    std::atomic<bool> try_to_expire_; 
    std::mutex mutex_; 
    std::condition_variable expired_cond_; 
};

#endif //KEEKOAIROBOT_TIMERTASKHELPER_H


void EchoFunc(const char* msg) 
{
    printf("%s\n", msg);
}

int main()
{
    Timer t;
    // 周期性执行定时任务
    t.StartTimer(1000, std::bind(EchoFunc, "hello c++11!"));
    std::this_thread::sleep_for(std::chrono::seconds(4)); 
    std::cout << "try to expire timer!" << std::endl; 
    t.Expire();

    // 异步执行一次定时任务
    t.AsyncWait(20000, std::bind(EchoFunc, "hello async!"));
    std::cout << "async wait." << std::endl;

    getchar();
}