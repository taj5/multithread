#include <typeinfo>
//    ITERATOR STUFF (from <iterator>)
//    ITERATOR TAGS (from <iterator>)
struct input_iterator_tag
    {    // identifying tag for input iterators   
    };
 
struct output_iterator_tag
    {    // identifying tag for output iterators
    };
 
struct forward_iterator_tag
    : input_iterator_tag, output_iterator_tag
    {    // identifying tag for forward iterators  这些继承关系有有效的IS-A
    };
 
struct bidirectional_iterator_tag
    : forward_iterator_tag
    {    // identifying tag for bidirectional iterators
    };
 
struct random_access_iterator_tag
    : bidirectional_iterator_tag
    {    // identifying tag for random-access iterators
    };


class A {
public:
    typedef random_access_iterator_tag iter_type;
};

class B
{
public:
    typedef bidirectional_iterator_tag iter_type;
};

#include <iostream>

using std::cout;
using std::endl;

int main()
{
    cout << typeid(A::iter_type).name()  << endl;
    cout << typeid(random_access_iterator_tag).name() << endl;
    cout << typeid(B).name() <<endl;
}