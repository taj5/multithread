#include <thread>
#include <queue>
#include <mutex>
#include <condition_variable>


std::queue<int> shared_queue;
std::mutex mtx;
std::condition_variable data_cond;

int produce()
{
    static int n = 0;
    if (++n > 10) {
        return 0;
    }
    return n;
}

void data_prepare_thread()
{
    printf("Enter prepare thread.\n");
    int n = produce();
    while(n) {
        printf("Prepare %d\n", n);
        std::lock_guard<std::mutex> lk(mtx);
        shared_queue.push(n);
        data_cond.notify_one();
        n = produce();
    }
}

// std::unique_lock<std::mutex>对象
// 1. 对象虽然不能复制，但具有移动属性，可以作为函数返回值，也可以放在容器中
// 2. 具备std::lock_guard的功能，还可以随时解锁49行，更加灵活
// 3. 等待中的线程必须解锁，这里智能用unique_lock, lock_guard不提供解锁功能
void data_process_thread()
{
    printf("Enter process thread.\n");
    while(true) {
        
        std::unique_lock<std::mutex> lk(mtx);
        // 1. wait等待条件为true直接返回，然后执行wait下面的语句。等待条件false,释放锁，并阻塞线程
        // 2. 等待条件可以传递lamda表达式，也可以传入callable类型的对象
        data_cond.wait(lk, []{
            return !shared_queue.empty();
        });
        int n = shared_queue.front();
        shared_queue.pop();
        lk.unlock();
        printf("Deal %d\n", n);
        if (n == 10) {
            break;
        }
    }
}

int main()
{
    printf("conditon_variable demo start.\n");
    std::thread t1(data_prepare_thread);
    std::thread t2(data_process_thread);

    t1.join();
    t2.join();

    printf("conditon_variable demo termiate.\n");
    return 1;
}