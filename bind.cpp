#include <string.h>
#include <iostream>
#include <map>
#include <list>
#include <functional>

#if 0
class Utils {
public:
    Utils(const char* name) {
        strcpy(_name, name);
    }
    
    void sayHello(const char* name) const {
        std::cout << _name << " say: hello " << name << std::endl;
    }
    
    static int getId() {
        return 10001;
    } 

    int operator()(int i, int j, int k) const {
        return i + j + k;
    }
    
private:
    char _name[32];
};

 
template<typename R = void, typename... Args>
class Fn {
public:
    Fn(std::function<R(Args...)> fun) : _fun(fun) {
    }
    
    R operator()(Args... args) {
        return _fun(args...);
    }
private:
    std::function<R(Args...) > _fun;
};
 
/*
 * 将函数注册到对象中，通过对象直接调用
 */
int main(void) {
 
    Utils util("Tom");
    Fn<void, const char*> sayHelloFn(std::bind(&Utils::sayHello, util, std::placeholders::_1));
    sayHelloFn("Jerry");
    
    Fn<int> getId1(std::bind(&Utils::getId));
    std::cout << getId1() << std::endl; 
    

    std::map<std::string, Fn<void, const char*>> func_map;

    return 0;
}
#endif

 
template<typename... Args>
class Fns
{
private:
 
	std::list<std::function<void(Args...)> > _calls;
 
public:
 
	virtual ~Fns()
	{
		_calls.clear();
	}
 
	void connect(std::function<void(Args...)> fct)
	{
		_calls.push_back(fct);
	}
 
	template<typename Object>
	void connect(Object* object, void (Object::*method)(Args...))
	{
		_calls.push_back([object,method](Args... args){(*object.*method)(args...);});
	}
 
	template<typename Object>
	void connect(Object* object, void (Object::*method)(Args...) const)
	{
		_calls.push_back([object,method](Args... args){(*object.*method)(args...);});
	}
 
	template<typename Object>
	void connect(const Object* object, void (Object::*method)(Args...) const)
	{
		_calls.push_back([object,method](Args... args){(*object.*method)(args...);});
	}
 
	void emit(Args... args)
	{
		for(auto call : _calls)
			call(args...);
	}
};


#include <cstdio>
 
class Foo
{
public:
 
	void bar(int x, int y)
	{
		printf("Foo::bar(%d, %d)\n", x, y);
	}
};
 
void foobar(int x, int y)
{
	printf("foobar(%d, %d)\n", x, y);
}
 
int main(void)
{

	Foo foo;
	Fns<int, int> s;
 
	// Connect a function
	s.connect(foobar);
	// Connect a class method
	s.connect(&foo, &Foo::bar);
	// Create and connect some lambda expression
	s.connect([&foo](int x){ 
		printf("lambda::"); //foo.bar(x, y); , int y 
	});
	// Emit the signal !
	s.emit(4, 2);
	getchar();
	return 0;
}