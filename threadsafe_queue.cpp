#include <thread>
#include <queue>
#include <mutex>
#include <condition_variable>
#include <memory>  // for std::shared_ptr<>

// 一个简单线程安全的队列
// try_push/try_pop/wait_and_pop/empty/size共五个接口

template<typename T>
class threadsafe_queue {
public:
    const size_t MAX_SIZE = 10;

    threadsafe_queue();
    threadsafe_queue(size_t size);
    threadsafe_queue(threadsafe_queue const& other);
    ~threadsafe_queue();

    // 这个接口设计不合适，push有可能失败
    //void push(T new_value);

    // 当队列满，push失败
    bool try_push(T new_value);

    bool try_pop(T& value);
    // 当队列空时，可返回空的shared_ptr对象
    std::shared_ptr<T> try_pop()
    {
        //TODO
    }

    void wait_and_pop(T& value);
    std::shared_ptr<T> wait_and_pop(){
        //TODO
    }

    bool empty() const;
    size_t size() const;
private:
    int size_;
    mutable std::mutex mtx_;
    std::condition_variable data_cond_;
    std::queue<T> shared_queue_;
};

template<typename T>
threadsafe_queue<T>::threadsafe_queue():size_(MAX_SIZE) {}

template<typename T>
threadsafe_queue<T>::threadsafe_queue(size_t size):size_(size) {}

template<typename T>
threadsafe_queue<T>::threadsafe_queue(threadsafe_queue const& other)
{
    std::lock_guard<std::mutex> lk(other.mtx_);
    shared_queue_ = other.shared_queue_;

}

template<typename T>
threadsafe_queue<T>::~threadsafe_queue() {}

template<typename T>
bool threadsafe_queue<T>::try_push(T new_value) 
{
    std::lock_guard<std::mutex> lk(mtx_);
    if (shared_queue_.size() >= size_) return false;
    shared_queue_.push(new_value);
    data_cond_.notify_all();
    return true;
}

template<typename T>
bool threadsafe_queue<T>::try_pop(T& value) 
{
    std::lock_guard<std::mutex> lk(mtx_);
    if (shared_queue_.empty()) return false;
    value = shared_queue_.front();
    shared_queue_.pop();
    return true;
}

template<typename T>
void threadsafe_queue<T>::wait_and_pop(T& value)
{
    std::unique_lock<std::mutex> lk(mtx_);
    data_cond_.wait(lk, [this]{
        return this->shared_queue_.empty() == false;
    });
    value = shared_queue_.front();
    shared_queue_.pop();
}

// 注意
// 1. 被声明为const的成员函数不能修改成员变量
// 2. mtx_作为成员变量，lock_guard锁定mtx_是一种可变修改
// 3. 基于以上两点， mtx_应被申明为mutable类型
template<typename T>
bool threadsafe_queue<T>::empty() const 
{
    std::lock_guard<std::mutex> lk(mtx_);
    return shared_queue_.empty();
}

template<typename T>
size_t threadsafe_queue<T>::size() const {
    std::lock_guard<std::mutex> lk(mtx_);
    return shared_queue_.size();
}

void spsc()
{
    threadsafe_queue<int> myq;
    std::thread p([&myq]{
        for (int i=0; i<20; i++) {
            bool ret = myq.try_push(i);
            if (!ret) printf("try push %d fail.\n", i);
            else printf("try push %d success.\n", i);
        }        
    });
    std::thread c([&myq]{
        while (true) {
            int v = 0;
            myq.wait_and_pop(v);
            printf("Pop %d\n", v);
            if (myq.empty()) break;
        }
    });


    p.join();
    c.join();

    int val;
    while (myq.try_pop(val)) {
        printf("Remain %d\n", val);
    }
}

int main()
{
    spsc();   
    return 0;
}